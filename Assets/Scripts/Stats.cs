﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour
{
    private const int MaxUpgrades = 3;
    public int CurrentHealth;

    // valores de los 4 niveles de cada stat

    int[] health = { 10, 13, 16, 20 };
    int[] defense = { 0, 1, 2, 3 };
    int[] attack = { 3, 5, 6, 9 };
    float[] speed = { 4, 9, 15, 20 };
    int[] jumpHeight = { 800, 1200, 1500, 1900};
    int[] shoot = {0, 1, 2, 3 };

    // nivel de upgrade actual de cada cosa
    int healthLevel = 0;
    int defenseLevel = 0;
    int attackLevel = 0;
    int speedLevel = 0;
    int jumpLevel = 0;
    int shootLevel = 0;

    // Start is called before the first frame update
    void Start()
    {
        CurrentHealth = health[0];
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ResetUpgrades()
    {
        healthLevel = 0;
        defenseLevel = 0;
        attackLevel = 0;
        speedLevel = 0;
        jumpLevel = 0;
        shootLevel = 0;
    }

    // INCREASERS
    // Llamar a estas funciones cuando quieras upgradear algo.
    // Si se upgradea algo, devuelve true. Si no se puede porque ya está al máximo, devuelve false.

    public bool IncreaseHealth()
    {
        if (healthLevel >= MaxUpgrades) return false;
        healthLevel++;

        return true;
    }

    public bool IncreaseDefense()
    {
        if (defenseLevel >= MaxUpgrades) return false;
        defenseLevel++;

        return true;
    }

    public bool IncreaseAttack()
    {
        if (attackLevel >= MaxUpgrades) return false;
        attackLevel++;

        return true;
    }

    public bool IncreaseSpeed()
    {
        if (speedLevel >= MaxUpgrades) return false;
        speedLevel++;

        return true;
    }

    public bool IncreaseJump()
    {
        if (jumpLevel >= MaxUpgrades) return false;
        jumpLevel++;

        return true;
    }

    public bool IncreaseShoot()
    {
        if (shootLevel >= MaxUpgrades) return false;
        shootLevel++;

        return true;
    }

    // te devuelve el valor actual de cada stat
    // si el nivel 2 de defensa es 5, devuelve 5 en GetDefenseValue()
    public int GetHealthValue() { return health[healthLevel]; }
    public int GetDefenseValue() { return defense[defenseLevel]; }
    public int GetAttackValue() { return attack[attackLevel]; }
    public float GetSpeedValue() { return speed[speedLevel]; }
    public int GetJumpValue() { return jumpHeight[jumpLevel]; }
    public int GetShootValue() { return shoot[shootLevel]; }

    //Devuelven el nivel de upgrade actual
    public int GetHealthLevel() { return healthLevel; }
    public int GetDefenseLevel() { return defenseLevel; }
    public int GetAttackLevel() { return attackLevel; }
    public int GetSpeedLevel() { return speedLevel; }
    public int GetJumpLevel() { return jumpLevel; }
    public int GetShootLevel() { return shootLevel; }
}
