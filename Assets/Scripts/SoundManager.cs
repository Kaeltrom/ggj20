﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager soundManager;

    public AudioSource sceneAudioSource;
    public AudioClip slowDown, speedUp;

    private void Awake()
    {
        if (soundManager != null)
        {
            Destroy(soundManager);
        }
        else
        {
            soundManager = this;
        }
    }

    public void PlaySlowDown()
    {
        sceneAudioSource.PlayOneShot(slowDown);
    }

    public void PlaySpeedUp()
    {
        sceneAudioSource.PlayOneShot(speedUp);
    }
}
