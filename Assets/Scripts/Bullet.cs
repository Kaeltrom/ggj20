﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public bool Player1;
    public Vector3 Dir;
    public float Velocity;
    public float LifeTime;
    private float elapsedtime = 0;
    public int Damage;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.position += (Dir * Velocity * Time.deltaTime);
        elapsedtime += Time.deltaTime;
        if(elapsedtime >= LifeTime)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter (Collider collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (collision.gameObject.GetComponent<PlayerController>().Player1 == Player1)
            {
                return;
            }
            Destroy(gameObject);
        }
    }

}
