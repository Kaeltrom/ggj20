﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TimerManager : MonoBehaviour
{
    public static TimerManager timerManagerInstance;

    public float initialTime = 30.0f;

    private TextMeshProUGUI timerText;
    private float remainingTime;

    private void Awake()
    {
        timerText = GetComponent<TextMeshProUGUI>();
        remainingTime = initialTime;

        if (timerManagerInstance != null)
        {
            Destroy(timerManagerInstance);
        }
        else
        {
            timerManagerInstance = this;
        }
    }

    private void Update()
    {
        if(remainingTime > 0 && !GameManager.gameManager.UpgradeMenuIsOpen)
        {
            ManageTimer();
        }
    }

    private void ManageTimer()
    {
        //First, substract the deltaTime from the remaining time
        remainingTime -= Time.deltaTime;

        if(remainingTime < 0)
        {
            remainingTime = 0;
        }

        string seconds;
        float secondsFloat;

        var timeRemaining = remainingTime;

        //First, store seconds in an aux variable to check if seconds has one or two digits
        secondsFloat = (timeRemaining % 60);

        seconds = (timeRemaining % 60).ToString("f0");

        //If secondsFloat has only one digit, add a Zero before it
        if (secondsFloat <= 9.5f)
        {
            seconds = "0" + seconds;
        }

        timerText.text = seconds;
    }

    //Use this in case we want to format the timer in minutes:seconds
    /*private void ManageTimer()
    {
        string minutes, seconds;
        float secondsFloat;

        var timeRemaining = remainingTime;

        //First, store seconds in an aux variable to check if seconds has one or two digits
        secondsFloat = (timeRemaining % 60);

        minutes = ((int)timeRemaining / 60).ToString();
        seconds = (timeRemaining % 60).ToString("f0");

        //If secondsFloat has only one digit, add a Zero before it
        if (secondsFloat <= 9.5f)
        {
            seconds = "0" + seconds;
        }

        if (secondsFloat >= 59.5f)
        {
            int auxMinutes = int.Parse(minutes);
            auxMinutes += 1;
            minutes = auxMinutes.ToString();

            seconds = "00";
        }

        timerText.text = minutes + ":" + seconds;
    }*/

    public void ResetTimer()
    {
        remainingTime = initialTime;
    }

    public void ResetTimer(float timeToSet)
    {
        remainingTime = timeToSet;
    }
}
