﻿using UnityEngine;
using System.Collections;

public class Mixer : MonoBehaviour
{
    public AudioSource S1;
    public AudioSource S2;

    float volume1 = 1.0f;
    float volume2 = 0.0f;

    bool last = false;




    // Use this for initialization
    void Start()
    {
        S1.Play();
        S1.loop = true;

        S2.Play();
        S2.loop = true;
    }

    // Update is called once per frame
    void Update()
    {
        S1.volume = volume1;
        S2.volume = volume2;


        if (last != GameManager.gameManager.UpgradeMenuIsOpen)
        {
            SwitchVolumes();
        }

        last = GameManager.gameManager.UpgradeMenuIsOpen;
    }

    void SwitchVolumes()
    {
        float temp = volume1;

        volume1 = volume2;
        volume2 = temp;
    }

}