﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBarManager : MonoBehaviour
{
    public Stats playerStats;

    public List<GameObject> healthParts = new List<GameObject>();

    private float currentLifePoints;

    private void Awake()
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            healthParts.Add(transform.GetChild(i).gameObject);
        }
    }

    private void Update()
    {
        if(currentLifePoints != playerStats.CurrentHealth)
        {
            UpdateHealthBar();
        }
    }

    public void SetMaxLifePoints()
    {
        currentLifePoints = playerStats.GetHealthValue();
        DrawLifePoints();
    }

    private void UpdateHealthBar()
    {
        foreach(GameObject healthPart in healthParts)
        {
            healthPart.SetActive(false);
        }

        currentLifePoints = playerStats.CurrentHealth;
        DrawLifePoints();
    }

    private void DrawLifePoints()
    {
        for (int i = 0; i < currentLifePoints; i++)
        {
            healthParts[i].SetActive(true);
        }
    }
}
