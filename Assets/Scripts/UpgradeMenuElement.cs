﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeMenuElement : MonoBehaviour
{
    public RobotParts upgradePart;
    public Button upgradeButton;
    public GameObject[] upgradeLevelBars;

    public void SetUpgradeElementValues(Stats playerToUpgradeStats, Color playerToUpgradeColor)
    {
        switch(upgradePart)
        {
            case RobotParts.Head:
                SetBarsStatus(playerToUpgradeStats.GetHealthLevel());
                break;
            case RobotParts.Torso:
                SetBarsStatus(playerToUpgradeStats.GetDefenseLevel());
                break;
            case RobotParts.Legs:
                SetBarsStatus(playerToUpgradeStats.GetJumpLevel());
                break;
            case RobotParts.Cannon:
                SetBarsStatus(playerToUpgradeStats.GetShootLevel());
                break;
            case RobotParts.Hand:
                SetBarsStatus(playerToUpgradeStats.GetAttackLevel());
                break;
            case RobotParts.Feet:
                SetBarsStatus(playerToUpgradeStats.GetSpeedLevel());
                break;
        }

        SetButtonHighlightColor(playerToUpgradeColor);
    }

    private void SetButtonHighlightColor(Color playerToUpgradeColor)
    {
        ColorBlock buttonColors = upgradeButton.colors;
        buttonColors.highlightedColor = playerToUpgradeColor;
        upgradeButton.colors = buttonColors;
    }

    //Upgrades the part assigned to the button selected by the winner
    public void UpgradeSelectedPart()
    {
        Stats playerToUpgradeStats = UpgradesManager.upgradesManager.GetPlayerToUpgradeStats();

        switch (upgradePart)
        {
            case RobotParts.Head:
                playerToUpgradeStats.IncreaseHealth();
                break;
            case RobotParts.Torso:
                playerToUpgradeStats.IncreaseDefense();
                break;
            case RobotParts.Legs:
                playerToUpgradeStats.IncreaseJump();
                break;
            case RobotParts.Cannon:
                playerToUpgradeStats.IncreaseShoot();
                break;
            case RobotParts.Hand:
                playerToUpgradeStats.IncreaseAttack();
                break;
            case RobotParts.Feet:
                playerToUpgradeStats.IncreaseSpeed();
                break;
        }

        //Set the upgrades UI as deactivated since an upgrade has been selected
        UpgradesManager.upgradesManager.upgradesMenuUI.SetActive(false);
        UpgradesManager.upgradesManager.healthAndTimeUI.SetActive(true);
        //Resets the input module values (just in case)
        UpgradesManager.upgradesManager.ResetInput();
        GameManager.gameManager.RestartMatch();
    }

    private void SetButtonStatus(bool enabled)
    {
        upgradeButton.interactable = enabled;
    }

    private void SetBarsStatus(int upgradeLevel)
    {
        //Sets the upgrade bar as active (or not) based on the upgrade level of the part
        for(int i = 0; i < upgradeLevelBars.Length; i++)
        {
            upgradeLevelBars[i].SetActive(upgradeLevel >= (i + 1));
        }

        //If the upgrade level is not the max level, then enable the upgrade part button
        SetButtonStatus(upgradeLevel != upgradeLevelBars.Length);
    }

    //Returns true if the last bar of the upgrade is active, meaning the upgrade is maxed
    public bool CheckUpgradeMaxed()
    {
        return upgradeLevelBars[upgradeLevelBars.Length - 1].activeSelf;
    }

    public enum RobotParts
    {
        Head,
        Torso,
        Legs,
        Cannon,
        Hand,
        Feet
    }
}
