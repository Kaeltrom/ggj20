﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Combat[] AttackHitboxes;
    public Animator anim;
    public bool Player1; // True if the left one at the beginnning
    public GameObject BalaBase;
    public GameObject BalaLvl1;
    public GameObject BalaLvl2;
    public GameObject BalaLvl3;
    public GameObject ExplosionSpike;
    public GameObject SparkHit;
    public LayerMask whatIsGround;
    Stats mStats;

    public int animState;


    public float JumpSpeed = 800;
    public float Speed = 100;
    public float MaxSpeed = 2;
    public float HorizontalDeceleration = 10;
    int BaseKnockBack = 1500;
    int BulletKnockBack = 850;

    public Vector2 playerDir;

    public float BeforeAttackTimer;
    public float AttackTime; //tiempo con la hitbox activa
    public float ExtraAttackTimer; // 

    private int JumpCount;

    public float BeforeShootTimer;
    public float ShootingTime;
    public float ExtraShootTimer; // 

    public float HurtInvulneravility;
    float elapsedattack = 20;
    float elapsedashoot = 0;
    public float CannonTime; //

    public bool isFloored;



    bool IsMoving = false;
    bool IsGrounded;
    bool VerticalDirection;
    bool IsAttacking;

    public float cldrRadius;
    bool IsThrowing;

    public Transform feetPos;
    public bool Hurt;
    public bool Stuned;
    float timer;
    public float Hitstuntime;
    Vector3 lastpos;

    Rigidbody rb;
    BoxCollider bx;

    string[] joystickNames;
    List<string> joystickNamesList = new List<string>();

    // Start is called before the first frame update
    void Start()
    {
        //Globalize this
        rb = GetComponent<Rigidbody>();
        bx = GetComponent<BoxCollider>();
        mStats = GetComponent<Stats>();

        AttackHitboxes = gameObject.GetComponentsInChildren<Combat>();
        DeactivateHitbox();
        lastpos = transform.position;

        mStats.ResetUpgrades();

        //Get the joysticks currently connected to the pc
        joystickNames = Input.GetJoystickNames();

        foreach(string joystick in joystickNames)
        {
            joystickNamesList.Add(joystick);
        }

        if (Player1)
            playerDir = Vector2.right;
        if (!Player1)
            playerDir = Vector2.left;
    }

    // Update is called once per frame
    void Update()
    {
        if(IsAttacking)
        {
            animState = 1;
        }
        else if (IsThrowing)
        {
            animState = 2;
        }
        else if (Hurt)
        {
            animState = 3;
        }
        else if (!IsGrounded)
        {
            animState = 4;
        }
        else if (IsMoving)
        {
            animState = 5;
        }
        else
        {
            animState = 0;
        }



        if (GameManager.gameManager.UpgradeMenuIsOpen)
        {
            GameManager.gameManager.ResetPlayerPositions();
            IsAttacking = false;
            DeactivateHitbox();
            gameObject.GetComponent<Renderer>().material.color = Color.white;
            elapsedattack = 20;
            rb.velocity = Vector3.zero;

            return;
        }

        if (GameManager.gameManager.gamePaused)
        {
            return;
        }

        UpdateStats();

        //Reset Z
        gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, 0);
        //Is IsAyyaj?


        //Is attacking?

        //Is hurt??

        if (!Stuned)
        {
            ToMove();
            Attack();
            Shoot();
            timer = 0;
        }
        else
        {
            if (timer >= Hitstuntime)
            {
                Stuned = false;
            }
            if(timer >= HurtInvulneravility)
            {
                Hurt = false;
            }
            timer += Time.deltaTime;
        }
        Deceleration();
        Animate();

        lastpos = transform.position;

        // Debug.Log(rb.velocity);
        // Debug.Log(IsGrounded);

    }

    void ToMove()
    {
        if (Player1)
        {
            if (Input.GetKeyDown(KeyCode.W) && IsGrounded)
            {
                rb.AddForce(Vector2.up * JumpSpeed);
            }

            if (!IsGrounded && (JumpCount < gameObject.GetComponent<Stats>().GetJumpLevel()) && Input.GetKeyDown(KeyCode.W))
            {
                rb.velocity = new Vector3(rb.velocity.x,0,rb.velocity.z);
                rb.AddForce(Vector2.up * JumpSpeed);
            }


            if (Input.GetKeyUp(KeyCode.W))
            {
                JumpCount++;
            }

            if (Input.GetKey(KeyCode.D))
            {
                rb.AddForce(Vector2.right * Speed);
                IsMoving = true;
                playerDir = Vector2.right;
            }
            else if (Input.GetKey(KeyCode.A))
            {
                rb.AddForce(Vector2.left * Speed);
                IsMoving = true;
                playerDir = Vector2.left;
            }
            else
                IsMoving = false;
        }

        else if (!Player1)
        {
            if(joystickNamesList.Contains("Aqui ira el Id del mando"))
            {
                if (Input.GetButtonDown("JumpXboxOne") && IsGrounded)
                {
                    rb.AddForce(Vector2.up * JumpSpeed);
                }

                if (Input.GetAxisRaw("HorizontalXboxOne") != 0)
                {
                    float h = 1 * Mathf.Sign(Input.GetAxisRaw("HorizontalXboxOne"));
                    IsMoving = true;
                    rb.AddForce(new Vector2(h, 0) * Speed);
                }
            }
            else
            {
                if(Input.GetKeyDown(KeyCode.I) && IsGrounded)
                {
                    rb.AddForce(Vector2.up * JumpSpeed);
                }


                if (!IsGrounded && (JumpCount < gameObject.GetComponent<Stats>().GetJumpLevel()) && Input.GetKeyDown(KeyCode.I))
                {
                    rb.AddForce(Vector2.up * JumpSpeed);
                }


                if (Input.GetKey(KeyCode.L))
                {
                    rb.AddForce(Vector2.right * Speed);
                    IsMoving = true;
                    playerDir = Vector2.right;
                }
                else if (Input.GetKey(KeyCode.J))
                {
                    rb.AddForce(Vector2.left * Speed);
                    IsMoving = true;
                    playerDir = Vector2.left;
                }
                else
                    IsMoving = false;
            }
        }

        else { IsMoving = false; }

        if (Player1 && Input.GetKey(KeyCode.LeftShift))
            IsAttacking = true;
        else if (!Player1 && Input.GetKey(KeyCode.RightShift))
            IsAttacking = true;
        else
            IsAttacking = false;

        if (Player1 && Input.GetKey(KeyCode.LeftShift))
            IsThrowing = true;
        else if (!Player1 && Input.GetKey(KeyCode.RightShift))
            IsThrowing = true;
        else
            IsThrowing = false;
    }

    void Deceleration()
    {
        if (rb.velocity.y < -0.1)
        {
            //rb.AddForce(new Vector2(0, -rb.velocity.y * DownMult));
        }
        if (rb.velocity.y > 0.1f)
        {
            if(Player1)
            {
                if(!Input.GetKey(KeyCode.W))
                {
                    rb.AddForce(new Vector2(0, -rb.velocity.y * 10));
                }
            }
            else
            {
                if (!Input.GetKey(KeyCode.I) && !Input.GetButton("JumpXboxOne"))
                {
                    rb.AddForce(new Vector2(0, -rb.velocity.y * 10));
                }
            }
        }

        if (!IsMoving)
        {
            rb.AddForce(new Vector2(-rb.velocity.x * HorizontalDeceleration, 0));
        }
        else
        {
            if (rb.velocity.x > MaxSpeed)
            {
                rb.velocity = new Vector3(MaxSpeed, rb.velocity.y, rb.velocity.z);
               Debug.Log("QUIETO");
            }
            else if (rb.velocity.x < -MaxSpeed)
            {
                rb.velocity = new Vector3(-MaxSpeed, rb.velocity.y, rb.velocity.z);
            }
        }

    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer.Equals(9))
        {
            IsGrounded = true;
            JumpCount = 0;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Hand") && other.transform.parent != this)
        {
            if (!Hurt)
            {
                float defense = BaseKnockBack;
                float defensemultiplier;

                if (mStats.GetDefenseValue() != 0)
                {
                    defensemultiplier = 1.0f / ((float)mStats.GetDefenseValue() + 1);
                }
                else
                {
                    defensemultiplier = 1.0f;
                }
                defense *= defensemultiplier;

                // knockback
                float dir = other.gameObject.transform.parent.GetComponent<PlayerController>().playerDir.x;

                var a = other.gameObject.transform.parent.GetComponent<PlayerController>();
                if(a)
                 if (!IsGrounded && !a.IsGrounded)
                    {
                        rb.AddForce(new Vector3(defense * dir, -defense * 8, 0));

                        var p = Instantiate(ExplosionSpike, gameObject.transform.position, Quaternion.FromToRotation(Vector3.zero, new Vector3(defense * dir, -defense * 8, 0)));
                        p.GetComponent<Explosion>().Play();

                    }
                    else
                    {
                        rb.AddForce(new Vector3(defense * dir, defense, 0));
                        var p = Instantiate(SparkHit, gameObject.transform.position, Quaternion.identity);
                        p.GetComponent<Explosion>().Play();

                    }

                else
                {
                    rb.AddForce(new Vector3(defense * dir, defense, 0));
                    var p = Instantiate(SparkHit, gameObject.transform.position, Quaternion.identity);
                    p.GetComponent<Explosion>().Play();
                }

                // deal damage
                ComputeDamage(other.gameObject.GetComponentInParent<Stats>().GetAttackValue());
            }

            Hurt = true;
            Stuned = true;
        }

        var c = (other.gameObject.GetComponent<Bullet>());
        if (c)
        {
            //deal dmg
            if (!Hurt && c.Player1 != Player1)
            {
                // knockback
                float dir = c.Dir.x;
                float defense = BaseKnockBack;
                float defensemultiplier;

                if (mStats.GetDefenseValue() != 0)
                {
                    defensemultiplier = 1.0f / ((float)mStats.GetDefenseValue() + 1);
                }
                else
                {
                    defensemultiplier = 1.0f;
                }
                defense *= defensemultiplier;

                // knockback
                rb.AddForce(new Vector3(defense * dir, defense, 0));
                {
                    var p = Instantiate(SparkHit, gameObject.transform.position, Quaternion.identity);
                    p.GetComponent<Explosion>().Play();

                }
                // deal damage
                ComputeDamage(c.GetComponent<Bullet>().Damage);
                Hurt = true;
                Stuned = true;
                Debug.Log("OUCH");
            }
        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.layer.Equals(9))
        {
            IsGrounded = false;
        }
    }


    void Animate()
    {/*
        anim.SetBool("IsMoving", IsMoving);
        anim.SetBool("IsGrounded", IsGrounded);
        anim.SetBool("VerticalDirection", VerticalDirection);
        anim.SetBool("IsAttacking", IsAttacking);
        anim.SetBool("IsThrowing", IsThrowing);*/
    }

    void Attack()
    {
        //Test for keys
        if (Player1 && Input.GetKeyDown(KeyCode.LeftShift))
        {
            IsAttacking = true;
        }
        else if (!Player1 && Input.GetKeyDown(KeyCode.RightShift))
        {
            IsAttacking = true;
        }
        else
        {
            IsAttacking = false;
        }
        //If so, you stay here for a while, but on air, where you can move
        //Then create a hitbox.
        if (IsAttacking)
        {
            if (elapsedattack >= AttackTime + ExtraAttackTimer + BeforeAttackTimer)
            {
                DeactivateHitbox();
                elapsedattack = 0;
            }
        }
        else
        {
            if (elapsedattack < BeforeAttackTimer)
            {
                gameObject.GetComponent<Renderer>().material.color = Color.grey;
               // Debug.Log("Preparation");
            }
            else if (elapsedattack > AttackTime + BeforeAttackTimer + ExtraAttackTimer)
            {
                gameObject.GetComponent<Renderer>().material.color = Color.white;
                DeactivateHitbox();
               // Debug.Log("Can Attack");
            }
            else if (elapsedattack > AttackTime + BeforeAttackTimer)
            {
                DeactivateHitbox();
                gameObject.GetComponent<Renderer>().material.color = Color.grey;
               // Debug.Log("Ending Lag");
            }
            else
            {
                ActivateHitbox();
                gameObject.GetComponent<Renderer>().material.color = Color.red;
                //Debug.Log("Attack");
            }

            elapsedattack += Time.deltaTime;


        }
        //Then the hitbox tests for the damage 
    }


    void Shoot()
    {
        //Test for keys
        if (Player1 && Input.GetKeyDown(KeyCode.Q))
        { 
            IsThrowing = true;
        }
        else if (!Player1 && Input.GetKeyDown(KeyCode.O))
        {
            IsThrowing = true;
        }
        else
        {
            IsThrowing = false;
        }
        //If so, you stay here for a while, but on air, where you can move
        //Then create a hitbox.
        if (IsThrowing)
        {
            if (elapsedashoot >= ShootingTime + ExtraShootTimer)
            {
                int i = gameObject.GetComponent<Stats>().GetShootLevel();

                switch (i)
                {
                    case (0):
                        {
                            var a = Instantiate(BalaBase, gameObject.transform.position, Quaternion.identity);
                            a.GetComponent<Bullet>().Player1 = Player1;
                            a.GetComponent<Bullet>().Dir = playerDir;
                            elapsedashoot = 0;
                            break;
                        }
                    case (1):
                        {
                            var a = Instantiate(BalaLvl1, gameObject.transform.position, Quaternion.identity);
                            a.GetComponent<Bullet>().Player1 = Player1;
                            a.GetComponent<Bullet>().Dir = playerDir;
                            elapsedashoot = 0;
                            break;
                        }
                    case (2):
                        {
                            var a = Instantiate(BalaLvl2, gameObject.transform.position, Quaternion.identity);
                            a.GetComponent<Bullet>().Player1 = Player1;
                            a.GetComponent<Bullet>().Dir = playerDir;
                            a.GetComponent<Bullet>().Damage++;

                            var dir = new Vector3(playerDir.x, playerDir.y, 0);

                            a = Instantiate(BalaLvl2, gameObject.transform.position + dir * 1.5f, Quaternion.identity);
                            a.GetComponent<Bullet>().Player1 = Player1;
                            a.GetComponent<Bullet>().Dir = playerDir;
                            a.GetComponent<Bullet>().Damage++;


                            a = Instantiate(BalaLvl2, gameObject.transform.position + dir * 3.0f, Quaternion.identity);
                            a.GetComponent<Bullet>().Player1 = Player1;
                            a.GetComponent<Bullet>().Dir = playerDir;
                            a.GetComponent<Bullet>().Damage++;
                            elapsedashoot = 0;
                            break;
                        }
                    case (3):
                        {
                            var a = Instantiate(BalaLvl3, gameObject.transform.position, Quaternion.identity);
                            a.GetComponent<Bullet>().Player1 = Player1;
                            a.GetComponent<Bullet>().Dir = playerDir;
                            
                            elapsedashoot = 0;
                            break;
                        }
                    default:
                        break;
                }

            }
        }
        else
        {
            if (elapsedashoot < BeforeShootTimer)
            {
                //Debug.Log("Preparation shoot");
            }
            else if (elapsedashoot > ShootingTime + BeforeShootTimer + ExtraShootTimer)
            {
                //Debug.Log("Idle");
            }
            else if (elapsedashoot > ShootingTime + BeforeShootTimer)
            {
                //Debug.Log("Ending shoot Lag");
            }
            else
            {
                //Debug.Log("Shoot");
            }
            elapsedashoot += Time.deltaTime;
        }
        //Then the hitbox tests for the damage 
    }

    void ActivateHitbox()
    {
        foreach (Combat c in AttackHitboxes)
        {
            if (playerDir.x > 0)
            {
                c.gameObject.transform.localPosition = new Vector3(1.5f, c.gameObject.transform.localPosition.y, c.gameObject.transform.localPosition.z);
            }
            else if (playerDir.x < 0)
            {
                c.gameObject.transform.localPosition = new Vector3(-1.5f, c.gameObject.transform.localPosition.y, c.gameObject.transform.localPosition.z);
            }
            c.gameObject.GetComponent<MeshRenderer>().enabled = true;
            c.gameObject.GetComponent<BoxCollider>().enabled = true;
        }
    }

    void DeactivateHitbox()
    {
        foreach (Combat c in AttackHitboxes)
        {
            c.gameObject.GetComponent<BoxCollider>().enabled = false;
            c.gameObject.GetComponent<MeshRenderer>().enabled = false;
        }
    }

    void ComputeDamage(int attackerValue)
    {
        mStats.CurrentHealth -= ((attackerValue - mStats.GetDefenseValue()) >= 0 ? (attackerValue - mStats.GetDefenseValue()) : 0);
    }

    void UpdateStats()
    {
        // Health is computed differently on the fly
        // Attack is computed differently on the fly
        // Defense is computed differently on the fly
        JumpSpeed = mStats.GetJumpValue();
        MaxSpeed = mStats.GetSpeedValue();

        // Debug.Log(MaxSpeed);
        // TODO Bullet
    }
}
