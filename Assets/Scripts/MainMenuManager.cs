﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    public GameObject mainMenuButtons;
    public GameObject creditsElements;

    public void LoadBattleScene()
    {
        SceneManager.LoadScene(1);
    }

    public void ShowCredits()
    {
        mainMenuButtons.SetActive(false);
        creditsElements.SetActive(true);
    }

    public void HideCredits()
    {
        mainMenuButtons.SetActive(true);
        creditsElements.SetActive(false);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
