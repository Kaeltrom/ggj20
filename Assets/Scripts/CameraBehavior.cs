﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehavior : MonoBehaviour
{
    public GameObject box;
    Camera cam;

    // Start is called before the first frame update
    void Start()
    {
         cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        MoveByDistance();
    }

    void MoveByFOV()
    {
        float x = Vector3.Distance(box.transform.position, cam.transform.position);
        float y = box.transform.localScale.y / 2f;
        float requiredFOV = Mathf.Atan(y / x) * Mathf.Rad2Deg;

        cam.fieldOfView = requiredFOV * 2f;
        cam.transform.position = new Vector3(box.transform.position.x, box.transform.position.y, this.transform.position.z);
    }

    void MoveByDistance()
    {
        float distance = 0.0f;

        var distanceH = -(box.transform.localScale.y) * 0.5f / Mathf.Tan(cam.fieldOfView * 0.5f * Mathf.Deg2Rad);
        var distanceW = -(box.transform.localScale.x / cam.aspect) * 0.5f / Mathf.Tan(cam.fieldOfView * 0.5f * Mathf.Deg2Rad);
        distance = distanceH < distanceW ? distanceH : distanceW;

        transform.position = new Vector3(box.transform.position.x, box.transform.position.y, distance);
    }
}
