﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicLibrary : MonoBehaviour
{
    public AudioClip[] MusicTracks;
    int CurrentTrack = 1;
    int TrackNum;
    int sw = 0;

    float mvolume;
    private AudioSource mAudio;

    // Start is called before the first frame update
    void Start()
    {
        mAudio = GetComponent<AudioSource>();
        mvolume = mAudio.volume;
        TrackNum = MusicTracks.Length;

        mAudio.clip = MusicTracks[CurrentTrack - 1];
    }

    // Update is called once per frame
    void Update()
    {
        if (mvolume != mAudio.volume)
        {
            sw++;
            if (sw >= 2)
            {
                Debug.Log("SWITCHING");
                sw = 0;
                CurrentTrack++;

                if (CurrentTrack > TrackNum) CurrentTrack = 1;

                mAudio.clip = MusicTracks[CurrentTrack - 1];
            }
        }

        mvolume = mAudio.volume;
    }
}
