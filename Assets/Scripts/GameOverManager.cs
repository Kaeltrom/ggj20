﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverManager : MonoBehaviour
{
    public GameObject gameOverPanel;
    public GameObject clonkText;
    public GameObject crashText;

    public void ShowGameOver(int winner)
    {
        gameOverPanel.SetActive(true);

        if (winner == 1)
        {
            clonkText.SetActive(true);
        }
        else
        {
            crashText.SetActive(true);
        }
    }
}
