using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager gameManager;

    public float timeToOpenUpgrades = 2.0f;
    public GameObject pausePanel;
    public GameObject P1;
    public GameObject P2;
    public float cameraUpgradesExtraSpace = 4.0f;
    public float cameraBattleExtraSpace = 14.0f;
    public HealthBarManager healthBarPlayer1, healthBarPlayer2;
    public bool gamePaused = false;
    public float TimeBeforeDeath;
    Vector3 RespawnP1;
    Vector3 RespawnP2;

    private int PlayerWhoLoses = 0;

    Stats StatsP1;
    Stats StatsP2;

    public int P1Victories = 0;
    public int P2Victories = 0;
    private float timer = 0;
    [HideInInspector]
    public bool UpgradeMenuIsOpen = false;
    private bool roundIsOver = false;

    private CameraScript cameraScript;
    private VictoriesBarManager victoriesBarManager;

    private void Awake()
    {
        if (gameManager != null)
        {
            Destroy(gameManager);
        }
        else
        {
            gameManager = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        RespawnP1 = P1.transform.position;
        RespawnP2 = P2.transform.position;

        StatsP1 = P1.GetComponent<Stats>();
        StatsP2 = P2.GetComponent<Stats>();

        cameraScript = FindObjectOfType<CameraScript>();
        healthBarPlayer1.SetMaxLifePoints();
        healthBarPlayer2.SetMaxLifePoints();

        victoriesBarManager = FindObjectOfType<VictoriesBarManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape) && !UpgradeMenuIsOpen)
        {
            Pause();
        }

        if(PlayerWhoLoses == 0)
             PlayerWhoLoses = DidAnyoneDie();
        if ((PlayerWhoLoses != 0) && !UpgradeMenuIsOpen && !roundIsOver)
        {
            roundIsOver = true;
            SoundManager.soundManager.PlaySlowDown();
            RotatePlayerOnDeath();

            //Camera zoom?
            //Particles??

            Time.timeScale = 0.2f;
            Invoke("RoundEnd", timeToOpenUpgrades * Time.timeScale);
        }
    }

    private void RotatePlayerOnDeath()
    {
        P1.GetComponent<Rigidbody>().useGravity = false;
        P2.GetComponent<Rigidbody>().useGravity = false;

        P1.GetComponent<Rigidbody>().velocity = Vector3.zero;
        P2.GetComponent<Rigidbody>().velocity = Vector3.zero;

        P1.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        P2.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        P1.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ;
        P2.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ;


        if ((PlayerWhoLoses == 1))
            P1.GetComponent<Rigidbody>().AddTorque(new Vector3(5 * P2.GetComponent<PlayerController>().playerDir.x, 0, 90));
        else
            P2.GetComponent<Rigidbody>().AddTorque(new Vector3(5 * P1.GetComponent<PlayerController>().playerDir.x, 0, 90));

        P1.GetComponent<PlayerController>().enabled = false;
        P2.GetComponent<PlayerController>().enabled = false;
    }

    private void RoundEnd()
    {
        Time.timeScale = 1.0f;
        SoundManager.soundManager.PlaySpeedUp();
        ResetPlayerPositions();
        victoriesBarManager.UpdateVictoryBars();

        if (CheckGameOver() != 0)
        {
            GameOver(CheckGameOver());
            return;
        }

        UpgradesManager.upgradesManager.OpenUpgradesMenu(PlayerWhoLoses);
        ResetPlayerPositions();
        PlayerWhoLoses = 0;
        UpgradeMenuIsOpen = true;
    }

    private void Pause()
    {
        //Checks if the time is stopped, and acts in consecuence
        if (Time.timeScale == 1)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }

        gamePaused = Time.timeScale == 0;
        pausePanel.SetActive(gamePaused);
    }

    public void ResetPlayerPositions()
    {
        P1.transform.position = RespawnP1;
        P2.transform.position = RespawnP2;

        //Set the camera extra space so the players are zoomed in
        cameraScript.ExtraSpace = cameraUpgradesExtraSpace;
    }

    int DidAnyoneDie()
    {
        if (StatsP1.CurrentHealth <= 0 && !roundIsOver)
        {
            P2Victories++;

            if(P1Victories > 0)
            {
                P1Victories = 0;
            }

            return 1;
        }

        if (StatsP2.CurrentHealth <= 0 && !roundIsOver)
        {
            P1Victories++;

            if (P2Victories > 0)
            {
                P2Victories = 0;
            }

            return 2;
        }

        return 0;
    }

    public void RestartMatch()
    {
        UpgradeMenuIsOpen = false;
        roundIsOver = false;
        StatsP1.CurrentHealth = StatsP1.GetHealthValue();
        StatsP2.CurrentHealth = StatsP2.GetHealthValue();

        //Set the camera extra space so the players are zoomed out
        UpgradeMenuIsOpen = false;

        StatsP1.CurrentHealth = StatsP1.GetHealthValue();
        StatsP2.CurrentHealth = StatsP2.GetHealthValue();

        P1.GetComponent<PlayerController>().enabled = true;
        P2.GetComponent<PlayerController>().enabled = true;

        P1.GetComponent<Rigidbody>().useGravity = true;
        P2.GetComponent<Rigidbody>().useGravity = true;


        P1.GetComponent<Rigidbody>().velocity = Vector3.zero;
        P2.GetComponent<Rigidbody>().velocity = Vector3.zero;
        P1.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        P2.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        P1.transform.rotation = Quaternion.Euler(0, 0, 0);
        P2.transform.rotation = Quaternion.Euler(0, 0, 0);

        P1.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
        P2.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;


        //Set the camera extra space so the players are zoomed out
        cameraScript.ExtraSpace = cameraBattleExtraSpace;

        //Reset healthbars and timer
        healthBarPlayer1.SetMaxLifePoints();
        healthBarPlayer2.SetMaxLifePoints();

        if(TimerManager.timerManagerInstance != null)
        {
            TimerManager.timerManagerInstance.ResetTimer();
        }
        // TODO other restart stuff.
    }

    private int CheckGameOver()
    {
        if(P1Victories >= 3)
        {
            return 1;
        }
        else if(P2Victories >= 3)
        {
            return 2;
        }

        return 0;
    }

    private void GameOver(int winner)
    {
        GameOverManager goManager = FindObjectOfType<GameOverManager>();

        goManager.ShowGameOver(winner);
        Invoke("LoadMainScene", 10);
    }

    private void LoadMainScene()
    {
        SceneManager.LoadScene(0);
    }
}