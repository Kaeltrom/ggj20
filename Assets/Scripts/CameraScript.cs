﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*  ## RECOMMENDED CAMERA SETTINGS ON THE EDITOR ##
 *  ===============================================
 *  
 *  P1 and P2 attached to their respective GameObjects
 *  Translation_Smoothness = 0.1
 *  Scaling_Smoothness = 0.05
 *  Extra Space = 10
 *  Camera Offset X = 0, Y = 1.
 */


public class CameraScript : MonoBehaviour
{
    // GameObjects to compare positions. It should be very easy to add more.
    public GameObject p1;
    public GameObject p2;

    // Camera Component
    public Camera CameraComponent;

    // Smoothness of the camera translation and scaling.
    // RECOMMENDED SETTING -> 0.1 and 0.05 respectively
    public float Translation_Smoothness = 2f;
    public float Scaling_Smoothness = 1f;

    // Extra space at all sides of the sreen. Like a 'zoom'.
    public float ExtraSpace = 10;

    // Offset to move the camera vertically or horizontally without affecting the scale.
    public Vector2 CameraOffset;

    // Position and scale to lerp to.
    Vector2 TargetPosition;
    Vector2 TargetScale;

    // Max and min values of all the gameobjects' coordinates, to calculate the size of the camera.
    float TopGO;
    float BottomGO;
    float LeftGO;
    float RightGO;

    // Distance vector between the most distant gameobjects.
    Vector2 Distance;

    // Multiplies the scale of the camera.
    float ScaleMultiplierX = 1;
    float ScaleMultiplierY = 1;

    Vector3 lastpos;


    void Start()
    {
        // Get the camera component (if any)
        // CameraComponent = GetComponent<Camera>();
        transform.localScale = new Vector3(16, 9, 1);
        lastpos = transform.position;
    }


    void Update()
    {
        // 1. Save and evaluate all the distances.
        EvaluatePositions();

        // 2. Calculate and smoothen the best camera position.
        CalculatePosition();

        // 3. Calculate and smoothen the best camera scale.
        CalculateScale();

        Smoothen();

        // Change the orthographic size, which is the scaleY coordinate divided by 2
        // (thanks Unity for that nonsense I guess)
        // CameraComponent.orthographicSize = transform.localScale.y / 2;
    }

    void EvaluatePositions()
    {
        // Calculate and save the max and min coordinates of the gameobjexts.
        // Should be easy to add more players.
        RightGO = Mathf.Max(p1.transform.position.x, p2.transform.position.x);
        LeftGO = Mathf.Min(p1.transform.position.x, p2.transform.position.x);
        TopGO = Mathf.Max(p1.transform.position.y, p2.transform.position.y);
        BottomGO = Mathf.Min(p1.transform.position.y, p2.transform.position.y);

        // Distance vector.
        // Add extra space to augmentate the distance and 'zoom out'
        Distance = new Vector2(RightGO - LeftGO, TopGO - BottomGO);
        Distance += new Vector2(ExtraSpace * 16 / 9, ExtraSpace);

        Debug.DrawLine(lastpos, transform.position, Color.white, 5, false);
        lastpos = transform.position;
    }

    void CalculatePosition()
    {
        // Camera will always look for the midpoint of both players.
        // !!!! THIS SHOULD BE MODIFIED FOR ADDING MORE PLAYERS !!!!
        Vector2 MidPoint = (p1.transform.position + p2.transform.position) / 2;

        // Set that position as a target
        TargetPosition = MidPoint + CameraOffset;
    }

    void CalculateScale()
    {
        // Calculate by how much we have to multiply the camera scale to fit
        // the players on the screen
        ScaleMultiplierX = (Distance.x / transform.localScale.x);
        ScaleMultiplierY = (Distance.y / transform.localScale.y);

        // Convert both distances to the same magnitude. Expand the scale
        // in the direction of the biggest movement change.
        // (It's 16/9 because the screen ratio is 16:9)
        if (Distance.x > Distance.y * 16/9)
        {
            TargetScale = transform.localScale * ScaleMultiplierX;
        }
        else
        {
            TargetScale = transform.localScale * ScaleMultiplierY;
        }




        // Old problematic stuff
        // transform.localScale += new Vector3(ExtraSpace * 16 / 9, ExtraSpace, 0);
    }

    void ComputeLimits()
    {

    }

    void Smoothen()
    {
        // Lerp  s m o o t h l y  to that target
        transform.position = new Vector3(Mathf.Lerp(transform.position.x, TargetPosition.x, Translation_Smoothness * Time.deltaTime),
                                         Mathf.Lerp(transform.position.y, TargetPosition.y, Translation_Smoothness * Time.deltaTime), 0);


        // Lerp to the target scale  s m o o t h l y.
        transform.localScale = new Vector3(Mathf.Lerp(transform.localScale.x, TargetScale.x, Scaling_Smoothness * Time.deltaTime),
                                   Mathf.Lerp(transform.localScale.y, TargetScale.y, Scaling_Smoothness * Time.deltaTime), 0.2f);
    }
}
