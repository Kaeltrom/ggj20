﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VictoriesBarManager : MonoBehaviour
{
    public Image playerOneVictoriesBar, playerTwoVictoriesBar;
    
    public void UpdateVictoryBars()
    {
        int victoriesP1, victoriesP2;

        victoriesP1 = GameManager.gameManager.P1Victories;
        victoriesP2 = GameManager.gameManager.P2Victories;

        if(victoriesP1 > victoriesP2)
        {
            switch(victoriesP1 - victoriesP2)
            {
                case 1:
                    playerOneVictoriesBar.fillAmount = 0.4f;
                    playerTwoVictoriesBar.fillAmount = 0f;
                    break;
                case 2:
                    playerOneVictoriesBar.fillAmount = 0.6f;
                    break;
                case 3:
                    playerOneVictoriesBar.fillAmount = 1;
                    break;
            }
        }
        else if(victoriesP1 < victoriesP2)
        {
            switch (victoriesP2 - victoriesP1)
            {
                case 1:
                    playerTwoVictoriesBar.fillAmount = 0.4f;
                    playerOneVictoriesBar.fillAmount = 0f;
                    break;
                case 2:
                    playerTwoVictoriesBar.fillAmount = 0.6f;
                    break;
                case 3:
                    playerTwoVictoriesBar.fillAmount = 1;
                    break;
            }
        }
    }
}
