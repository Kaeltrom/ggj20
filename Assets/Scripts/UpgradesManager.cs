﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UpgradesManager : MonoBehaviour
{
    public static UpgradesManager upgradesManager;

    public Color playerOneColor, playerTwoColor;
    public GameObject healthAndTimeUI;
    public GameObject upgradesMenuUI;
    public GameObject playerOneArrow, playerTwoArrow;
    public Stats playerOneStats;
    public Stats playerTwoStats;
    public EventSystem eventSystem;
    public StandaloneInputModule standAloneInputModule;


    public UpgradeMenuElement[] upgradeElementsArray;

    private List<Button> upgradeButtons = new List<Button>();
    private Color playerToUpgradeColor;
    private Stats playerToUpgradeStats;
    private string inputHorizontalString, inputVerticalString;

    private void Awake()
    {
        if(upgradesManager != null)
        {
            Destroy(upgradesManager);
        }
        else
        {
            upgradesManager = this;
        }
    }

    private void Start()
    {
        if(eventSystem == null)
        {
            eventSystem = FindObjectOfType<EventSystem>();
        }

        if(standAloneInputModule == null)
        {
            standAloneInputModule = FindObjectOfType<StandaloneInputModule>();
        }

        inputHorizontalString = standAloneInputModule.horizontalAxis;
        inputVerticalString = standAloneInputModule.verticalAxis;
    }

    //This funcion is called to open the upgrades menu on ending a round
    public void OpenUpgradesMenu(int playerToUpgrade)
    {
        upgradesMenuUI.SetActive(true);
        healthAndTimeUI.SetActive(false);

        playerOneStats.CurrentHealth = playerOneStats.GetHealthValue();
        playerTwoStats.CurrentHealth = playerTwoStats.GetHealthValue();

        var P1 = playerOneStats.gameObject;
        var P2 = playerTwoStats.gameObject;

        P1.GetComponent<PlayerController>().enabled = true;
        P2.GetComponent<PlayerController>().enabled = true;

        P1.GetComponent<Rigidbody>().useGravity = true;
        P2.GetComponent<Rigidbody>().useGravity = true;


        P1.GetComponent<Rigidbody>().velocity = Vector3.zero;
        P2.GetComponent<Rigidbody>().velocity = Vector3.zero;
        P1.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        P2.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        P1.transform.rotation = Quaternion.Euler(0, 0, 0);
        P2.transform.rotation = Quaternion.Euler(0, 0, 0);

        P1.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
        P2.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;

        if (playerToUpgrade == 1)
        {
            playerToUpgradeStats = playerOneStats;
            playerToUpgradeColor = playerOneColor;
            playerOneArrow.SetActive(true);
            playerTwoArrow.SetActive(false);

            //Set the input to detect only player two keys
            SetPlayerTwoInput();
        }
        else
        {
            playerToUpgradeStats = playerTwoStats;
            playerToUpgradeColor = playerTwoColor;
            playerOneArrow.SetActive(false);
            playerTwoArrow.SetActive(true);
        }

        //Show the amount of levels each upgrade has, and disable maxed upgrade buttons
        SetUpgradesCurrentStatus();

        //After setting the values, we check if ALL the upgrades are maxed. If they are, we go directly to the next round
        if (AreUpgradesMaxed())
        {
            //Llamar al GameManager y empezar la siguiente ronda
            upgradesMenuUI.SetActive(false);
            healthAndTimeUI.SetActive(true);
            ResetInput();
            GameManager.gameManager.RestartMatch();
        }
    }

    private void SetUpgradesCurrentStatus()
    {
        foreach(UpgradeMenuElement menuElement in upgradeElementsArray)
        {
            menuElement.SetUpgradeElementValues(playerToUpgradeStats, playerToUpgradeColor);
        }
    }

    public Stats GetPlayerToUpgradeStats()
    {
        return playerToUpgradeStats;
    }

    private bool AreUpgradesMaxed()
    {
        foreach(UpgradeMenuElement menuElement in upgradeElementsArray)
        {
            //If any of the upgrades is not maxed, return false;
            if(menuElement.CheckUpgradeMaxed() == false)
            {
                //We set the first not maxed upgrade to be the first selected button
                eventSystem.SetSelectedGameObject(menuElement.upgradeButton.gameObject);

                return false;
            }
        }

        //Else, we return true since all upgrades are maxed for this player
        return true;
    }

    private void SetPlayerTwoInput()
    {
        standAloneInputModule.horizontalAxis = inputHorizontalString + "P2";
        standAloneInputModule.verticalAxis = inputVerticalString + "P2";
    }

    public void ResetInput()
    {
        standAloneInputModule.horizontalAxis = inputHorizontalString;
        standAloneInputModule.verticalAxis = inputVerticalString;
    }
}
 