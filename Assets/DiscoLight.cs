﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiscoLight : MonoBehaviour
{

    public float minOnTime;
    public float minOfTime;
    public float MaxOnTime;
    public float MaxOfTime;


    public float OnTime;
    public float OfTime;

    private float elapsedtime;
    public bool On;
    private Color col;

    // Start is called before the first frame update
    void Start()
    { 
    }

    // Update is called once per frame
    void Update()
    {
        if(On)
        {
            elapsedtime += Time.deltaTime;

            gameObject.GetComponent<Light>().enabled = true;
            if (elapsedtime >= MaxOnTime)
            {
                //Create new MaxTime 


                On = false;
                elapsedtime = minOfTime;
                GetRandomColor();
            }
        }
        else
        {
            elapsedtime += Time.deltaTime;
            gameObject.GetComponent<Light>().color = col;
            gameObject.GetComponent<Light>().enabled = false;
            if (elapsedtime >= MaxOfTime)
            {
                On = true;
                elapsedtime = minOnTime;
            }
        }

    }

    void GetRandomColor()
    {
        col = Random.ColorHSV();
    }
}
